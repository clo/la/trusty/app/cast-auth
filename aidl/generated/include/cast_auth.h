/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#pragma once

#define CAST_AUTH_PORT "com.android.trusty.cast_auth"

__BEGIN_CDECLS

/**
 * cast_auth_ProvisionKey()
 *
 * @param req_payload
 *
 * @return: 0 on success, or an error code < 0 on failure.
 */
int cast_auth_ProvisionKey(const ::tidl::Payload& req_payload);

/**
 * cast_auth_SignHash()
 *
 * @param req_payload
 * @param[out] resp_payload
 * @param[out] resp_payload_buf_size: size of the output resp_payload array
 * @param[out] resp_payload_size: number of bytes written into the output array
 *
 * @return: 0 on success, or an error code < 0 on failure.
 */
int cast_auth_SignHash(const ::tidl::Payload& req_payload,
                       uint8_t* resp_payload,
                       size_t resp_payload_buf_size,
                       size_t* resp_payload_size);

__END_CDECLS
